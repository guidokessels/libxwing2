#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// deck
enum class Dck {
  CoreSet,
};

class DeckNotFound : public std::runtime_error {
 public:
  DeckNotFound(Dck d);
  DeckNotFound(std::string d);
};

class Deck {
 public:
  static Deck              GetDeck(Dck d);
  static Deck              GetDeck(std::string d);
  static std::vector<Deck> GetAllDecks();
  Dck         GetType() const;
  std::string GetXws()  const;
  std::string GetName() const;

 private:
  Dck type;
  std::string xws;
  std::string name;

  static std::vector<Deck> deck;

  Deck(Dck         t,
       std::string x,
       std::string n);
};



// trait
enum class Trt {
  Pilot,
  Ship,
};

class TraitNotFound : public std::runtime_error {
 public:
  TraitNotFound(Trt d);
};

class Trait {
 public:
  static Trait GetTrait(Trt d);
  Trt          GetType() const;
  std::string  GetName() const;

 private:
  Trt type;
  std::string name;

  static std::vector<Trait> trait;

  Trait(Trt         t,
        std::string n);
};



// Damage Condition
enum class Dmg {
  // Core set
  PanickedPilot,
  BlindedPilot,
  WoundedPilot,
  StunnedPilot,
  ConsoleFire,
  DamagedEngine,
  WeaponsFailure,
  HullBreach,
  StructuralDamage,
  DamagedSensorArray,
  LooseStabilizer,
  DisabledPowerRegulator,
  FuelLeak,
  DirectHit
};



typedef std::pair<Dmg,Dck> DmgCrd;



// damage deck
class DamageCardNotFound : public std::runtime_error {
 public:
  DamageCardNotFound(DmgCrd dc);
};

class DamageCard {
 public:
  static DamageCard              GetDamageCard(DmgCrd dc);
  static std::vector<DamageCard> GetAllDamageCards(Dck d);
  uint8_t     GetNumber() const;
  Dmg         GetDamage() const;
  Deck        GetDeck()   const;
  Trait       GetTrait()  const;
  uint8_t     GetCount()  const;
  std::string GetName()   const;
  std::string GetText()   const;

 private:
  uint8_t     number;
  Dmg         damage;
  Dck         deck;
  Trt         trait;
  uint8_t     count;
  std::string name;
  std::string text;

  static std::vector<DamageCard> damageCards;

  DamageCard(uint8_t     num,
	     Dmg         dmg,
	     Dck         dck,
             Trt         trt,
             uint8_t     cou,
             std::string nam,
             std::string txt);
};

}
