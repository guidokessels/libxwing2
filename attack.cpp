#include "attack.h"

namespace libxwing2 {

FiringArcNotFound::FiringArcNotFound(Arc a) : runtime_error("Firing Arc not found (enum " + std::to_string((int)a) + ")") { }

std::vector<FiringArc> FiringArc::firingArcs = {
  { Arc::None,         "None"         },
  { Arc::Front,        "Front"        },
  { Arc::Rear,         "Rear"         },
  { Arc::Bullseye,     "Bullseye"     },
  { Arc::FullFront,    "FullFront"    },
  { Arc::FullRear,     "FullRear"     },
  { Arc::SingleTurret, "SingleTurret" },
  { Arc::DoubleTurret, "DoubleTurret" },

};

FiringArc FiringArc::GetFiringArc(Arc typ) {
  for(FiringArc a : FiringArc::firingArcs) {
    if(a.GetType() == typ) {
      return a;
    }
  }
  throw FiringArcNotFound(typ);
}

Arc         FiringArc::GetType()      const { return this->type; }
std::string FiringArc::GetName()      const { return this->name; }

FiringArc::FiringArc(Arc         t,
                     std::string n)
  : type(t), name(n) { }
}
