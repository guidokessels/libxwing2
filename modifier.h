#pragma once
#include "action.h"
#include "attack.h"
#include "maneuver.h"
#include "upgradetype.h"
#include <functional>
#include <experimental/optional>

namespace libxwing2 {

class Modifier {
 public:
  struct ManeuverDifficulty {
    std::experimental::optional<int8_t> speed;
    Brn    bearing;
  };

  Modifier();
  Modifier(int16_t h, int16_t s);
  Modifier(ActionBar add);
  Modifier(std::vector<UpT> add);
  Modifier(std::vector<UpT> add, std::vector<UpT> rem);
  Modifier(PriAttacks add);
  Modifier(ActionBar adda, std::vector<UpT> addu);
  Modifier(std::vector<UpT> addu, int16_t s);
  Modifier(std::vector<ManeuverDifficulty> m);

  int16_t GetHullMod();
  int16_t GetShieldMod();

  const ActionBar& GetAddedActions();

  const std::vector<UpT>& GetAddedSlots();
  const std::vector<UpT>& GetRemoveedSlots();

  const PriAttacks& GetAddedAttacks();

  const std::vector<ManeuverDifficulty>& GetDecreasedDifficultyManeuvers();

 private:
  int16_t hull;
  int16_t shield;

  ActionBar addedActions;
  
  std::vector<UpT> addedSlots;
  std::vector<UpT> removedSlots;

  PriAttacks addedAttacks;

  std::vector<ManeuverDifficulty> decreaseDif;
};

}
