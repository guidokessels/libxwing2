#include "upgradetype.h"

namespace libxwing2 {

// *** UpgradeType ***
UpgradeTypeNotFound::UpgradeTypeNotFound(UpT u)           : runtime_error("UpgradeType not found: (enum " + std::to_string((int)u) + ")") { }
UpgradeTypeNotFound::UpgradeTypeNotFound(std::string xws) : runtime_error("UpgradeType not found: '" + xws + "'") { }

std::vector<UpgradeType> UpgradeType::upgradeTypes = {
  { UpT::Astromech,    "Astromech Droid", "AMD"    },
  { UpT::Cannon,       "Cannon",          "Cann"   },
  { UpT::Config,       "Configuration",   "Cfg"    },
  { UpT::Crew,         "Crew",            "Crew"   },
  { UpT::Device,       "Device",          "Device" },
  { UpT::Force,        "Force",           "Force"  },
  { UpT::Gunner,       "Gunner",          "Gun"    },
  { UpT::Illicit,      "Illicit",         "Illi"   },
  { UpT::Missile,      "Missile",         "Misl"   },
  { UpT::Modification, "Modification",    "Mod"    },
  { UpT::System,       "System",          "Sys"    },
  { UpT::Talent,       "Talent",          "Tal"    },
  { UpT::Title,        "Title",           "Title"  },
  { UpT::Torpedo,      "Torpedo",         "Trp"    },
  { UpT::Turret,       "Turret",          "Tur"    },
};

UpgradeType UpgradeType::GetUpgradeType(UpT typ) {
  for(UpgradeType u : UpgradeType::upgradeTypes) {
    if(u.GetType() == typ) {
      return u;
    }
  }
  throw UpgradeTypeNotFound(typ);
}

std::vector<UpgradeType> UpgradeType::GetAllUpgradeTypes() { return UpgradeType::upgradeTypes; }

UpT         UpgradeType::GetType()       const { return this->type; }
std::string UpgradeType::GetName()       const { return this->name; }
std::string UpgradeType::GetShortName()  const { return this->shortName; }

UpgradeType::UpgradeType(UpT         t,
                         std::string n,
                         std::string s)
  : type(t), name(n), shortName(s) { }

UpT operator|(UpT a, UpT b) {
  return static_cast<UpT>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

UpT operator&(UpT a, UpT b) {
  return static_cast<UpT>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}
}
