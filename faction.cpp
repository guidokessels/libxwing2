#include "faction.h"

namespace libxwing2 {

// Faction
std::vector<Faction> Faction::factions = {
//{ Fac::None,       "None",              "None"        },
  { Fac::Rebel,      "Rebel Alliance",    "Rebel"       },
  { Fac::Imperial,   "Galactic Empire",   "Imperial"    },
  { Fac::Scum,       "Scum and Villainy", "Scum"        },
  { Fac::Resistance, "Resistance",        "Resistance"  },
  { Fac::FirstOrder, "First Order",       "First Order" },
//{ Fac::All,        "All",               "All"         },
};

Fac operator|(Fac a, Fac b) {
  return static_cast<Fac>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

Fac operator&(Fac a, Fac b) {
  return static_cast<Fac>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}

FactionNotFound::FactionNotFound(Fac f)           : runtime_error("Faction not found (enum " + std::to_string((int)f) + ")") { }
FactionNotFound::FactionNotFound(std::string xws) : runtime_error("Faction not found (" + xws + ")") { }

Faction Faction::GetFaction(Fac typ) {
  for(Faction f : Faction::factions) {
    if(f.GetType() == typ) {
      return f;
    }
  }
  throw FactionNotFound(typ);
}

std::vector<Faction> Faction::GetAllFactions() { return Faction::factions; }

Fac         Faction::GetType()  const { return this->type; }
std::string Faction::GetName()  const { return this->name; }
std::string Faction::GetShort() const { return this->shortName; }

Faction::Faction(Fac         t,
                 std::string n,
		 std::string s)
  : type(t), name(n), shortName(n) { }

}
