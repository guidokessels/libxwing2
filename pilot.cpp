#include "pilot.h"
#include "release.h"

namespace libxwing2 {



// ship ability
ShipAbility::ShipAbility(std::string n, std::string t)
  : name(n), text(t) { }

std::string ShipAbility::GetName() const { return this->name; }
std::string ShipAbility::GetText() const { return this->text; }



PilotNotFound::PilotNotFound(std::string p, std::string f, std::string s) : runtime_error("Pilot not found: '" + p + "/" + f + "/" + s + "'") { }
PilotNotFound::PilotNotFound(Plt p)                                       : runtime_error("Pilot not found (enum " + std::to_string((int)p) + ")") { }

// factories
Pilot Pilot::GetPilot(Plt plt) {
  for(Pilot &ps : Pilot::pilots) {
    if(ps.GetType() == plt) {
      return ps;
    }
  }
  throw PilotNotFound(plt);
}
#if 0
Pilot Pilot::GetPilot(std::string pilot, std::string faction, std::string ship) {
  for(Pilot &ps : Pilot::pilots) {
    if(/*(ps.GetXws() == pilot) &&*/ (ps.GetFaction().GetType() <= Faction::GetFaction(faction).GetType()) /*&& (ps.GetShip().GetXws() == ship)*/) {
      return ps;
    }
  }
  throw PilotNotFound(pilot, faction, ship);
}
#endif
std::vector<Pilot> Pilot::FindPilot(std::string p, Fac f, std::vector<Pilot> src) {
  std::vector<Pilot> ret;
  std::string ss = ToLower(p); // searchString
  for(Pilot &ps : src) {
    if((ps.GetFaction().GetType() & f) == ps.GetFaction().GetType()) {
      if(/*(ToLower(ps.GetXws()).find(ss)       != std::string::npos) ||*/
	 (ToLower(ps.GetName()).find(ss)      != std::string::npos) ||
	 (ToLower(ps.GetShortName()).find(ss) != std::string::npos)
	 ) {
	ret.push_back(ps);
      }
    }
  }
  return ret;
}

std::vector<Pilot> Pilot::GetAllPilots(Fac faction) {
  if(faction == Fac::All) { return Pilot::pilots; }
  std::vector<Pilot> ret;
  for(Pilot p : Pilot::pilots) {
    if(p.GetFaction().GetType() == faction) {
      ret.push_back(p);
    }
  }
  return ret;
}

// maintenance
void Pilot::SanityCheck() {
#if 0
  typedef std::tuple<std::string, std::string, std::string> Entry;
  std::vector<Entry> entries;
  std::vector<Entry> dupes;
  int counter=0;
  printf("Checking Pilots");
  for(Pilot p : pilots) {
    counter++;
    Entry e = Entry{""/*p.GetXws()*/, p.GetFaction().GetXws(), ""/*p.GetShip().GetXws()*/ };
    if(std::find(entries.begin(), entries.end(), e) == entries.end()) {
      printf("."); fflush(stdout);
    } else {
      dupes.push_back(e);
      printf("X"); fflush(stdout);
    }
    entries.push_back(e);
  }
  printf("DONE\n");
  printf("Pilots: %zu\n", entries.size());
  printf("Dupes: %zu\n", dupes.size());
  if(dupes.size()) {
    for(Entry e : dupes) {
      printf("  %s - %s - %s\n", std::get<0>(e).c_str(), std::get<1>(e).c_str(), std::get<2>(e).c_str());
    }
  }
#endif
}



  //std::string Pilot::GetXws()        const { return this->pilotXws; }
Plt         Pilot::GetType()       const { return this->pilot; }
std::string Pilot::GetName()       const { return this->sides[this->curSide].pilotName; }
std::string Pilot::GetShortName()  const { return this->sides[this->curSide].pilotNameShort; }
std::string Pilot::GetSubtitle()   const { return this->sides[this->curSide].pilotSubtitle; }
Faction     Pilot::GetFaction()    const { return Faction::GetFaction(this->faction); }
Ship        Pilot::GetShip()       const { return Ship::GetShip(this->ship); }
std::experimental::optional<ShipAbility> Pilot::GetShipAbility() const { return this->sides[this->curSide].shipAbility; }
bool        Pilot::HasAbility()    const { return this->sides[this->curSide].hasAbility; }
std::string Pilot::GetText()       const { return this->sides[this->curSide].text; }
uint8_t     Pilot::GetLimited()    const { return this->limited; }
bool        Pilot::IsUnreleased()  const {
  for(const Release& r : Release::GetAllReleases()) {
    if(!r.IsUnreleased()) {
      for(Plt p : r.GetPilots()) {
	if(p == this->GetType()) {
	  return false;
	}
      }
    }
  }
  return true;
}

int8_t Pilot::GetNatInitiative() const { return this->initiative; }
int8_t Pilot::GetModInitiative() const {
  int8_t init = this->GetNatInitiative();
  return init;
}

PriAttacks Pilot::GetNatAttacks() const { return this->sides[this->curSide].attacks; }
PriAttacks Pilot::GetModAttacks() const {
  PriAttacks pa = this->GetNatAttacks();
  return pa;
}

int8_t Pilot::GetNatAgility() const { return this->sides[this->curSide].agility; }
int8_t Pilot::GetModAgility() const {
  int8_t agility = this->GetNatAgility();
  return agility;
}

int8_t Pilot::GetNatHull() const { return this->sides[this->curSide].hull; }
int8_t Pilot::GetModHull() const {
  int8_t hull = this->GetNatHull();
  return hull;
}

int8_t Pilot::GetNatShield() const { return this->sides[this->curSide].shield; }
int8_t Pilot::GetModShield() const {
  int8_t shield = this->GetNatShield();
  return shield;
}

Chargeable Pilot::GetNatCharge() const { return this->sides[this->curSide].charge; }
Chargeable Pilot::GetModCharge() const {
  Chargeable charge = this->GetNatCharge();
  return charge;
}

Chargeable Pilot::GetNatForce() const { return this->sides[this->curSide].force; }
Chargeable Pilot::GetModForce() const {
  Chargeable force = this->GetNatForce();
  return force;
}

int16_t Pilot::GetNatCost() const { return 0; /*this->sides[this->curSide].cost;*/ }
int16_t Pilot::GetModCost() const {
  int16_t cost = this->GetNatCost();
  return cost;
}

ActionBar Pilot::GetNatActions() const { return this->sides[this->curSide].actions; }
ActionBar Pilot::GetModActions() const {
  ActionBar a = this->GetNatActions();
  return a;
}

Maneuvers Pilot::GetNatManeuvers() const { return this->GetShip().GetManeuvers(); }
Maneuvers Pilot::GetModManeuvers() const {
  Maneuvers m = this->GetNatManeuvers();
  return m;
}

std::vector<Upg> Pilot::GetNatPossibleUpgrades() const { return {}; }
std::vector<Upg> Pilot::GetModPossibleUpgrades() const { return {}; }



Pilot::Pilot(Plt         t,
	     Fac         f,
	     Shp         s,
	     uint8_t     l,
	     int8_t      i,
	     int16_t     c,
	     PilotSide   p)
  : pilot(t), faction(f), ship(s), limited(l), initiative(i), cost(c), curSide(0), sides({p}) { }

}
