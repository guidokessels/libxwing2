#include "shared.h"

namespace libxwing2 {

// misc helpers
std::string ToLower(std::string s) {
  std::string ret;
  ret.resize(s.length());
  std::transform(s.begin(), s.end(), ret.begin(), [](char c){ return std::tolower(c); });
  return ret;
}

}
