#include "token.h"
#include <algorithm>

namespace libxwing2 {

// token
std::vector<Token> Token::tokens = {
  // game effects
  { Tok::Calculate,             "Calculate",               "Calculate"     },
  { Tok::Cloak,                 "Cloak",                   "Cloak"         },
  { Tok::Crit,                  "Critical Hit",            "Crit"          },
  { Tok::Disarm,                "Disarm",                  "Disarm"        },
  { Tok::Evade,                 "Evade",                   "Evade"         },
  { Tok::Focus,                 "Focus",                   "Focus"         },
  { Tok::ForceCharge,           "Force Charge",            "Force"         },
  { Tok::ID,                    "ID",                      "ID"            },
  { Tok::Ion,                   "Ion",                     "Ion"           },
  { Tok::Jam,                   "Jam",                     "Jam"           },
  { Tok::Lock,                  "Lock",                    "Lock"          },
  { Tok::Reinforce,             "Reinforce",               "Reinforce"     },
  { Tok::Shield,                "Shield",                  "Shield"        },
  { Tok::StandardCharge,        "Standard Charge",         "Charge"        },
  { Tok::Stress,                "Stress",                  "Stress"        },
  { Tok::Tractor,               "Tractor",                 "Tractor"       },

  // obstacles
  { Tok::Asteroid,              "Asteroid",                "Asteroid"      },
  { Tok::DebrisCloud,           "Debris Cloud",            "Debris"        },

  // devices
  { Tok::ProtonBomb,            "Proton Bomb",             "Proton Bomb"   },
  { Tok::ProximityMine,         "Proximity Mine",          "ProxMine"      },
  { Tok::SeismicCharge,         "Seismic Charge",          "SeismicCh"     },

  // conditions
  { Tok::Hunted,                "Hunted",                  "Hunted"        },
  { Tok::ListeningDevice,       "Listening Device",        "Listening Dev" },
  { Tok::SuppressiveFire,       "Suppressive Fire",        "Supp. Fire"    },

  // misc
  { Tok::FirstPlayerMarker,     "First Player Marker",     "First Player"  },
  { Tok::HyperspaceMarker,      "Hyperspace Marker",       "Hyperspace"    },

  /*

  { Tok::ConnerNet,             "Conner Net",              "Conner Net"     },
  { Tok::IonBomb,               "Ion Bomb",                "Ion Bomb"       },
  { Tok::ClusterMine,           "Cluster Mine",            "Clstr Mine"     },
  { Tok::ThermalDetonator,      "Thermal Detonator",       "Thermal Det"    },
  { Tok::Bomblet,               "Bomblet",                 "Bomblet"        },

  { Tok::ISYTDS,                "ISYTDS",                  "ISYTDS"         },
  { Tok::FanaticalDevotion,     "Fanatical Devotion",      "Fanatical Dev"  },
  { Tok::ADebtToPay,            "A Debt To Pay",           "A Debt To Pay"  },
  { Tok::Rattled,               "Rattled",                 "Rattled"        },
  { Tok::Scrambled,             "Scrambled",               "Scrambled"      },
  { Tok::OptimizedPrototype,    "Optimized Prototype",     "Opt. Proto."    },
  */
};

TokenNotFound::TokenNotFound(Tok t) : runtime_error("Token not found (enum " + std::to_string((int)t) + ")") { }

Token Token::GetToken(Tok tt) {
  for(Token t : Token::tokens) {
    if(t.GetType() == tt) {
      return t;
    }
  }
  throw TokenNotFound(tt);
}

Tok         Token::GetType()      const { return this->type; }
std::string Token::GetName()      const { return this->name; }
std::string Token::GetShortName() const { return this->shortName; }

Token::Token(Tok         t,
             std::string n,
             std::string s)
  : type(t), name(n), shortName(s) { }



// tokens
Tokens::Tokens(Tok t, uint16_t c) : type(t), count(c) { }
Tokens::~Tokens() { }
Tok      Tokens::GetType()  { return this->type;  }
uint16_t Tokens::GetCount() { return this->count; }

LkToken::LkToken(uint8_t n)
  : Tokens(Tok::Lock,1), number(n) {}
uint8_t LkToken::GetNumber() { return this->number; }

IDTokens::IDTokens(uint8_t n)
  : Tokens(Tok::ID, 3), number(n) { }
uint8_t IDTokens::GetNumber() { return this->number; }



// tokencollection
TokenCollection::TokenCollection(std::vector<std::shared_ptr<Tokens>> t) : tokens(t) { }

std::vector<Tok> TokenCollection::GetTokenTypes() {
  std::vector<Tok> ret;
  for(std::shared_ptr<Tokens>& t : this->tokens) {
    ret.push_back(t->GetType());
  }
  std::sort(ret.begin(), ret.end());
  auto last = std::unique(ret.begin(), ret.end());
  ret.erase(last, ret.end());
  return ret;
}

uint16_t TokenCollection::GetTokenCount(Tok tt) {
  uint16_t ret=0;
  for(std::shared_ptr<Tokens>& t : this->tokens) {
    if(t->GetType() == tt) {
      ret += t->GetCount();
    }
  }
  return ret;
}

std::vector<uint8_t> TokenCollection::GetLockTokens() {
  std::vector<uint8_t> ret;
  for(std::shared_ptr<Tokens>& t : this->tokens) {
    if(t->GetType() == Tok::Lock) {
      if(LkToken *lt = dynamic_cast<LkToken*>(t.get())) {
        ret.push_back(lt->GetNumber());
      }
    }
  }
  std::sort(ret.begin(), ret.end());
  return ret;
}

std::vector<uint8_t> TokenCollection::GetIdTokens() {
  std::vector<uint8_t> ret;
  for(std::shared_ptr<Tokens>& t : this->tokens) {
    if(t->GetType() == Tok::ID) {
      if(IDTokens* idt = dynamic_cast<IDTokens*>(t.get())) {
        ret.push_back(idt->GetNumber());
      }
    }
  }
  std::sort(ret.begin(), ret.end());
  return ret;
}


  
}
