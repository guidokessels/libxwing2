#pragma once
#include "action.h"
#include "attack.h"
#include "faction.h"
#include "shared.h"
#include "ship.h"

namespace libxwing2 {

class Pilot;

typedef std::vector<Shp> Ships;

enum class Plt;
enum class Upg;
typedef std::vector<Arc> Arcs;
typedef std::pair<std::vector<Plt>, std::vector<Upg>> Allowed;
typedef std::function<std::vector<std::string>(const Pilot&, const std::vector<Pilot>&)> RestrictionCheck;



class Restriction {
 public:
  Restriction();
  Restriction(std::string t, Fac f);
  Restriction(std::string t, BSz b);
  Restriction(std::string t, Ships s);
  Restriction(std::string t, SAct a);
  Restriction(std::string t, Arcs a);
  Restriction(std::string t, Fac f, SAct a);
  Restriction(std::string t, Fac f, Allowed a);
  Restriction(std::string t, BSz b, SAct a);
  Restriction(std::string t, Fac f, Ships s);

  std::string GetText();
  std::vector<std::string> CheckRestrictions(const Pilot&, const std::vector<Pilot>&); 

 private:
  const std::string text;
  RestrictionCheck restCheck;
};

}
