#pragma once
#include "action.h"
#include "chargeable.h"
#include "maneuver.h"
#include "modifier.h"
#include "restriction.h"
#include "shared.h"
#include "upgradetype.h"
#include <experimental/optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// Misc
 class Pilot;
 // typedef std::function<void(Pilot&, std::vector<Pilot>&)> Prepper;



// Upgrade
enum class Upg {
  // astromech
  ChopperA, Genius, R2Astro, R2D2, R3Astro, R4Astro, R5Astro, R5D8, R5P8, R5TK,

  // cannon
  HLC, IonCan, JamBeam, TracBeam,

  // config
  Os1ArsenalLdt, PvtWing, SFoils, Xg1AssaultCfg,

  // crew
  AdmSloane,       AgentKallus,     BFett,           BMalbus,    C3PO,       CAndor,     CBane,     ChewieReb,     ChewieScum, ChopperC,
  CRee,            CVizago,         DVader,          DeathTr,    DirKrennic, EmpPalp,    FLSlicer,  FourLOM,       Gonk,       GrandInq,
  GrandMoffTarkin, HSolo,           HSyndulla,       IG88D,      Informant,  ISBSlicer,  Jabba,     JErso,         KJarrus,    KOnyo,
  L337,            LCalrissianReb,  LCalrissianScum, LOrgana,    LRazzi,     Maul,       MinTua,    MoffJerjerrod, MYarro,     NNunb,
  NovTech,         PerCPilot,       Qira,            R2D2C,      SWren,      SGerrera,   SeasonNav, SeventhSister, TactOff,    TBeckett,
  TripleZero,      UPlutt,          ZOrrelios,       Zuckuss,

  // device
  Bomblet, ConnerNet, ProtonBmb, ProxMine, SeismicCh,

  // force
  HeightPerc, InstAim, Sense, SNReflex,

  // gunner
  Bistan,    Bossk,      BT1,       Dengar, EBridger, FifthBrother, Greedo, HSoloG, HsGunner, LSkywalkerG,
  SkBombard, VetTailGun, VetTurret,

  // illicit
  CloakDev, ContraCyb, DeadmansSw, FeedbackAr, InertDamp, RigCargoCh,

  // mod
  AblatPlat,  AdvSLAM, Afterburn, ElectBaff, EngUpg, HullUpg, MuniFailSa, StaDcVanes, ShieldUpg, StealthDev,
  TactScramb,

  // missile
  BarRockets, ClustMsl, ConcusMsl, HomingMsl, IonMsl, PRockets,

  // system
  AdvSensors, CollisDet, FCS, TrajSim,

  // talent
  CrackShot, Daredevil, DebGambit, Elusive,  ExpHan,   Fearless, Intimidat, Juke,    LoneWolf, Marksman,
  Outmaneuv, Predator,  Ruthless,  SatSalvo, Selfless, SquadLdr, SwarmTac,  TrickSh,

  // title
  Andrasta,  Dauntless, Ghost,   Havoc,        HoundsTooth,  IG2000, LandosMF, Marauder, MilFalcon, MistHunter,
  MoldyCrow, Outrider,  Phantom, PunishingOne, ShadowCaster, SlaveI, ST321,    Virago,

  // torpedo
  AdvProtTrp, IonTrp, ProtTrp,

  // turret
  DorsalTrt, IonCanTrt,
};

class UpgradeNotFound : public std::runtime_error {
 public:
  UpgradeNotFound(Upg u);
};

struct UpgradeSide {
  const UpT                                    upgradeType;
  const std::vector<UpT>                       slots;
  const std::string                            title;
  const std::string                            titleShort;
  const Chargeable                             charge;
  const Chargeable                             force;
  const std::experimental::optional<SecAttack> attackStats;
  const Modifier                               modifier;
  const Restriction                            restriction;
  const bool                                   hasAbility;
  const std::string                            text;
};

class Upgrade {
 public:
  // factories
  static Upgrade GetUpgrade(Upg upgrade);
  static std::vector<Upgrade> FindUpgrade(std::string u, std::vector<Upgrade> src=Upgrade::upgrades);
  static std::vector<Upgrade> GetAllUpgrades();

  // maintenance
  static void SanityCheck();

  // info (card)
  Upg         GetType()            const;
  std::string GetName()            const;
  uint8_t     GetLimited()         const;
  bool        IsUnreleased()       const;

  // info (side)
  UpgradeType      GetUpgradeType() const;
  std::vector<UpT> GetSlots()       const;
  std::string      GetTitle()       const;
  std::string      GetShortTitle()  const;
  Chargeable       GetCharge()      const;
  Chargeable       GetForce()       const;
  std::experimental::optional<SecAttack> GetAttackStats()    const;
  Modifier         GetModifier()        const;
  Restriction      GetRestriction()     const;
  bool             HasAbility()         const;
  std::string      GetText()            const;

  // gamestate
  bool IsEnabled() const;
  void Enable();
  void Disable();
  bool IsDualSided() const;
  void Flip();

 private:
  const Upg         type;
  const std::string name;
  const uint8_t     limited;

  bool isEnabled;

  uint8_t curSide;
  std::vector<UpgradeSide> sides;

  static std::vector<Upgrade> upgrades;

  Upgrade(Upg         upg,
	  std::string nam,
	  uint8_t     lim,
	  UpgradeSide s1);

  Upgrade(Upg         upg,
	  std::string nam,
	  uint8_t     lim,
	  UpgradeSide s1,
	  UpgradeSide s2);
};
}
