#include "modifier.h"

namespace libxwing2 {

Modifier::Modifier() {}
Modifier::Modifier(int16_t h, int16_t s) : hull(h), shield(s) {}
Modifier::Modifier(ActionBar add) : addedActions(add) {}
Modifier::Modifier(std::vector<UpT> add) : addedSlots(add) {}
Modifier::Modifier(std::vector<UpT> add, std::vector<UpT> rem) : addedSlots(add), removedSlots(rem) {}
Modifier::Modifier(PriAttacks add) : addedAttacks(add) {}
Modifier::Modifier(ActionBar adda, std::vector<UpT> addu) : addedActions(adda), addedSlots(addu) {}
Modifier::Modifier(std::vector<UpT> addu, int16_t s) : shield(s), addedSlots(addu) {};
Modifier::Modifier(std::vector<ManeuverDifficulty> m) : decreaseDif(m) {};

int16_t Modifier::GetHullMod()   { return this->hull; }
int16_t Modifier::GetShieldMod() { return this->shield; }

const ActionBar& Modifier::GetAddedActions()   { return this->addedActions; }

const std::vector<UpT>& Modifier::GetAddedSlots()    { return this->addedSlots; }
const std::vector<UpT>& Modifier::GetRemoveedSlots() { return this->removedSlots; }

const PriAttacks& Modifier::GetAddedAttacks()    { return this->addedAttacks; }

const std::vector<Modifier::ManeuverDifficulty>& Modifier::GetDecreasedDifficultyManeuvers() { return this->decreaseDif; }

}
