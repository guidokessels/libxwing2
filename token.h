#pragma once
#include <memory>
#include <string>
#include <vector>
#include <stdint.h>

namespace libxwing2 {

enum class Tok : uint16_t {
  // game effects
  Calculate         = 0,
  Cloak,
  Crit,
  Disarm,
  Evade,
  Focus,
  ForceCharge,
  ID,
  Ion,
  Jam,
  Lock,
  Reinforce,
  Shield,
  StandardCharge,
  Stress,
  Tractor,

  // obstacles
  Asteroid          = 128,
  DebrisCloud,

  // devices
  //ClusterMine,
  //ConnerNet,
  //IonBomb,
  ProtonBomb        = 256,
  ProximityMine,
  SeismicCharge,
  //ThermalDetonator,
  //Bomblet,

  // conditions
  Hunted            = 384,
  ListeningDevice,
  SuppressiveFire,
  //ISYTDS,
  //FanaticalDevotion,
  //ADebtToPay,
  //Rattled,
  //Scrambled,
  //OptimizedPrototype,

  // misc
  FirstPlayerMarker = 512,
  HyperspaceMarker,

};

class TokenNotFound : public std::runtime_error {
 public:
  TokenNotFound(Tok t);
};

class Token {
 public:
  static Token GetToken(Tok t);
  Tok         GetType()      const;
  std::string GetName()      const;
  std::string GetShortName() const;

 private:
  Tok type;
  std::string name;
  std::string shortName;

  static std::vector<Token> tokens;

  Token(Tok         t,
        std::string name,
        std::string shortName);
};



// tokens
class Tokens {
 public:
  Tokens(Tok t, uint16_t c);
  virtual ~Tokens();
  Tok GetType();
  uint16_t GetCount();
 private:
  Tok type;
  uint16_t count;
};

class LkToken : public Tokens {
 public:
  LkToken(uint8_t n);
  uint8_t GetNumber();
 private:
  uint8_t number;
};

class IDTokens : public Tokens {
 public:
  IDTokens(uint8_t n);
  uint8_t GetNumber();
 private:
  uint8_t number;
};



// tokencollection
class TokenCollection {
 public:
  TokenCollection(std::vector<std::shared_ptr<Tokens>> t);
  std::vector<Tok> GetTokenTypes();
  uint16_t GetTokenCount(Tok t);
  std::vector<uint8_t> GetLockTokens();
  std::vector<uint8_t> GetIdTokens();
 private:
  std::vector<std::shared_ptr<Tokens>> tokens;
};





}
