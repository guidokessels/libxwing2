#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// UpgradeType
enum class UpT {
  Astromech,
  Cannon,
  Config,
  Crew,
  Device,
  Force,
  Gunner,
  Illicit,
  Missile,
  Modification,
  System,
  Talent,
  Title,
  Torpedo,
  Turret,
};

typedef std::vector<UpT> UpgradeBar;

class UpgradeTypeNotFound : public std::runtime_error {
 public:
  UpgradeTypeNotFound(UpT u);
  UpgradeTypeNotFound(std::string xws);
};

class UpgradeType {
 public:
  static UpgradeType GetUpgradeType(UpT typ);
  static std::vector<UpgradeType> GetAllUpgradeTypes();
  UpT         GetType()       const;
  std::string GetName()       const;
  std::string GetShortName()  const;

 private:
  UpT type;
  std::string name;
  std::string shortName;

  static std::vector<UpgradeType> upgradeTypes;

  UpgradeType(UpT         t,
              std::string n,
              std::string s);
};
UpT operator|(UpT a, UpT b);
UpT operator&(UpT a, UpT b);

};
