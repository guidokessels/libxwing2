#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

enum class Cnd {
  Hunted,
  LstnDev,
  OptProt,
  SupFire,
};

class ConditionNotFound : public std::runtime_error {
 public:
  ConditionNotFound(Cnd t);
};

class Condition {
 public:
  static Condition GetCondition(Cnd c);
  static std::vector<Condition> GetAllConditions();

  Cnd         GetType()      const;
  uint8_t     GetLimited()   const;
  std::string GetName()      const;
  std::string GetText()      const;
  bool        IsUnreleased() const;

 private:
  Cnd         type;
  uint8_t     limited;
  std::string name;
  std::string text;

  static std::vector<Condition> conditions;

  Condition(Cnd         t,
	    uint8_t     l,
	    std::string n,
	    std::string txt);
};

}
