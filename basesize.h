#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// Base Size
enum class BSz {
  None   = 0x00,
  Small  = 0x01,
  Medium = 0x02,
  Large  = 0x04,
  Huge   = 0x08,
  All    = 0x0F
};

BSz operator|(BSz a, BSz b);
BSz operator&(BSz a, BSz b);

class BaseSizeNotFound : public std::runtime_error {
 public:
  BaseSizeNotFound(BSz b);
};

class BaseSize {
 public:
  static BaseSize GetBaseSize(BSz s);
  BSz         GetType()      const;
  std::string GetName()      const;

 private:
  BSz type;
  std::string name;

  static std::vector<BaseSize> baseSizes;

  BaseSize(BSz         t,
           std::string n);
};

}
