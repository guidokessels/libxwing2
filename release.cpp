#include "release.h"
#include <ctime>

namespace libxwing2 {

ReleaseNotFound::ReleaseNotFound(Rel r)         : runtime_error("Release not found (enum " + std::to_string((int)r) + ")") { }
ReleaseNotFound::ReleaseNotFound(std::string r) : runtime_error("Release not found (" + r + ")") { }
/*
std::vector<Release> Release::GetPilot(std::string plt, Fac fac, Shp shp) {
  std::vector<Release> ret;
  for(Release r : Release::releases) {
    for(RelPilot p : r.pilots) {
      if((p.name == plt) && (p.faction == fac) && (p.ship == shp)) {
        ret.push_back(r);
        break;
      }
    }
  }
  return ret;
}



std::vector<Release> Release::GetUpgrade(Upg type, std::string name) {
  std::vector<Release> ret;
  for(Release r : Release::releases) {
    for(RelUpgrade u : r.upgrades) {
      if((u.type == type) && (u.name == name)) {
        ret.push_back(r);
        break;
      }
    }
  }
  return ret;
}
*/

Release Release::GetRelease(Rel rel) {
  for(Release r : Release::releases) {
    if(r.GetType() == rel) {
      return r;
    }
  }
  throw ReleaseNotFound(rel);
}

Release Release::GetRelease(std::string sku) {
  for(Release r : Release::releases) {
    if(r.GetSku() == sku) {
      return r;
    }
  }
  throw ReleaseNotFound(sku);
}

std::vector<Release> Release::GetByPilot(Plt p) {
  std::vector<Release> ret;
  for(Release r : Release::releases) {
    for(Plt ps : r.pilots) {
      if(ps == p) {
        ret.push_back(r);
        break;
      }
    }
  }
  return ret;
}

std::vector<Release> Release::GetByUpgrade(Upg u) {
  std::vector<Release> ret;
  for(Release r : Release::releases) {
    for(Upg us : r.upgrades) {
      if(us == u) {
        ret.push_back(r);
        break;
      }
    }
  }
  return ret;
}

std::vector<Release> Release::GetAllReleases() { return Release::releases; }

Rel                      Release::GetType()            const { return this->release; }
std::string              Release::GetSku()             const { return this->sku;  }
std::string              Release::GetIsbn()            const { return this->isbn; }
std::string              Release::GetName()            const { return this->name; }
RelGroup                 Release::GetGroup()           const { return this->group; }
RelDate                  Release::GetAnnounceDate()    const { return this->announceDate; }
RelDate                  Release::GetReleaseDate()     const { return this->releaseDate; }
std::string              Release::GetAnnouncementUrl() const { return this->urls.announcement; }
std::vector<std::string> Release::GetPreviewUrls()     const { return this->urls.previews; }
std::string              Release::GetReleaseUrl()      const { return this->urls.release; }
std::vector<RelShip>     Release::GetShips()           const { return this->ships; }
std::vector<Plt>         Release::GetPilots()          const { return this->pilots; }
std::vector<Upg>         Release::GetUpgrades()        const { return this->upgrades; }
std::vector<Cnd>         Release::GetConditions()      const { return this->conditions; }
TokenCollection          Release::GetTokens()          const { return this->tokens; }
bool                     Release::IsUnreleased()       const {

  if(this->GetReleaseDate().year == 0
     && this->GetReleaseDate().month == 0
     && this->GetReleaseDate().date == 0) {
    return true;
  }
  time_t now = std::time(0);
  time_t rel;
  {
    std::tm tm = {0};
    tm.tm_year = this->releaseDate.year - 1900;
    tm.tm_mon  = this->releaseDate.month - 1;
    tm.tm_mday = this->releaseDate.date;
    rel = mktime(&tm);
  }
  return rel > now;
}

Release::Release(Rel                                  rel,
		 std::string                          sku,
                 std::string                          isbn,
                 std::string                          nam,
                 RelGroup                             grp,
                 RelDate                              ad,
                 RelDate                              rd,
                 RelUrls                              url,
                 std::vector<RelShip>                 shps,
                 std::vector<Plt>                     plts,
                 std::vector<Upg>                     upgs,
                 std::vector<Cnd>                     cond,
                 std::vector<std::shared_ptr<Tokens>> tok)
: release(rel), sku(sku), isbn(isbn), name(nam), group(grp), announceDate(ad), releaseDate(rd), urls(url),
  ships(shps), pilots(plts), upgrades(upgs), conditions(cond), tokens(tok) { }

};
