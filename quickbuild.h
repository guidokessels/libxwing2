#pragma once
#include <stdint.h>
#include <vector>
#include "pilot.h"
#include "upgrade.h"

namespace libxwing2 {

class QuickBuildNotFound : public std::runtime_error {
 public:
  QuickBuildNotFound(Plt p);
};

class QuickBuild {
 public:
  static QuickBuild              GetQuickBuild(Plt t);
  static std::vector<QuickBuild> GetAllQuickBuilds();

  Plt                  GetPlt()      const;
  Pilot                GetPilot()    const;
  uint8_t              GetThreat()   const;
  std::vector<Upgrade> GetUpgrades() const;

 private:
  Plt pilot;
  uint8_t threat;
  std::vector<Upg> upgrades;

  static std::vector<QuickBuild> quickBuilds;

  QuickBuild(Plt              p,
	     uint8_t          t,
	     std::vector<Upg> u);
};

}
