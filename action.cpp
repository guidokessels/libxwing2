#include "action.h"

namespace libxwing2 {

// Actions
std::vector<Action> Action::actions = {
  { Act::None,       "None",        "None"   },
  { Act::Focus,      "Focus",       "Focus"  },
  { Act::Lock,       "Lock",        "Lock"   },
  { Act::BarrelRoll, "Barrel Roll", "BRoll"  },
  { Act::Reload,     "Reload",      "Reload" },
  { Act::Reinforce,  "Reinforce",   "Rforce" },
  { Act::Boost,      "Boost",       "Boost"  },
  { Act::Coordinate, "Coordinate",  "Coord"  },
  { Act::Jam,        "Jam",         "Jam"    },
  { Act::Evade,      "Evade",       "Evade"  },
  { Act::Cloak,      "Cloak",       "Cloak"  },
  { Act::RotateArc,  "Rotate Arc",  "RotArc" },
  { Act::Calculate,  "Calculate",   "Calc"   },
  { Act::SLAM,       "SLAM",        "SLAM"   },
  //{ Act::Recover,    "Recover",     "Rec"    },

};


Act operator|(Act a, Act b) {
  return static_cast<Act>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

Act operator&(Act a, Act b) {
  return static_cast<Act>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}

Act operator+(Act a, Act b) {
  return static_cast<Act>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

Act operator-(Act a, Act b) {
  return static_cast<Act>(static_cast<uint32_t>(a) & ~static_cast<uint32_t>(b));
}

ActionNotFound::ActionNotFound(Act a) : runtime_error("Action not found (enum " + std::to_string((int)a) + ")") { }

Action Action::GetAction(Act typ) {
  for(Action a : Action::actions) {
    if(a.GetType() == typ) {
      return a;
    }
  }
  throw ActionNotFound(typ);
}

Act         Action::GetType()      const { return this->type; }
std::string Action::GetName()      const { return this->name; }
std::string Action::GetShortName() const { return this->shortName; }

Action::Action(Act         t,
               std::string n,
               std::string s)
  : type(t), name(n), shortName(s) { }


void ForEachAction(Act actions, std::function<void(Act)>f) {
  for(int i=0; i<sizeof(Act)*8; i++) {
    uint32_t v = (1<<i) & (uint32_t)actions;
    if(v) {
      Act a = (Act)v;
      f(a);
    }
  }
}

bool operator ==(const SAct a, const SAct b) {
  return (a.difficulty == b.difficulty) && (a.action == b.action);
}
  
}
