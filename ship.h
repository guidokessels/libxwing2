#pragma once
#include "basesize.h"
#include "maneuver.h"
#include "shared.h"
#include <experimental/optional>
#include <stdexcept>
#include <vector>


namespace libxwing2 {




// ship
enum class Shp {
  Aggressor,
  AlphaClass,
  ARC170,
  AttackShuttle,
  Auzituck,
  BWing,
  EscCraft,
  EWing,
  FangFighter,
  Firespray,
  G1A,
  HWK290,
  JM5K,
  Kihraxz,
  Kimogila,
  KWing,
  Lambda,
  LancerClass,
  M3A,
  Quadjumper,
  RZ1AWing,
  Scurrg,
  Sheathipede,
  StarViper,
  T65XWing,
  TIEAdvV1,
  TIEAdvX1,
  TIEAggressor,
  TIEBomber,
  TIEDefender,
  TIEFighter,
  TIEInterceptor,
  TIEPhantom,
  TIEPunisher,
  TIEReaper,
  TIEStriker,
  UWing,
  VCX100,
  VT49,
  YT1300scum,
  YT1300reb,
  YT2400,
  YV666,
  YWing,
  Z95,
};

class ShipNotFound : public std::runtime_error {
 public:
  ShipNotFound(Shp s);
};

class Ship {
 public:
  static Ship GetShip(Shp s);
  static std::vector<Ship> FindShip(std::string s);
  static std::vector<Ship> GetAllShips();

  Shp         GetType()       const;
  std::string GetName()       const;
  std::string GetShortName()  const;
  BaseSize    GetBaseSize()   const;
  Maneuvers   GetManeuvers()  const;

 private:
  Shp type;
  std::string name;
  std::string shortName;
  BSz         baseSize;
  Maneuvers   maneuvers;

  static std::vector<Ship> ships;

  Ship(Shp         t,
       std::string n,
       std::string s,
       BSz         b,
       Maneuvers   m);
};

}
