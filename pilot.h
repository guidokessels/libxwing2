#pragma once
#include "action.h"
#include "attack.h"
#include "chargeable.h"
#include "faction.h"
#include "ship.h"
#include "upgrade.h"
#include <list>
#include <vector>

namespace libxwing2 {

// ship ability
class ShipAbility {
 public:
  ShipAbility(std::string n, std::string t);

  std::string GetName() const;
  std::string GetText() const;

 private:
  const std::string   name;
  const std::string   text;
};

// pilot
enum class Plt {

  // Aggressor (Scum)
  IG88A, IG88B, IG88C, IG88D,
  // ARC170 (Rebel)
  NWexley_ARC, GDreis_ARC, SBey, Ibtisam,
  // AttackShuttle (Rebel)
  HSyndulla_AS, EBridger_AS, SWren_AS, ZOrrelios_AS,
  // Auzituck (Rebel)
  Wullffwarro, Lowhhrick, KashyyykDef,
  // BWing (Rebel)
  BStramm, TNumb, BladeSqVet, BlueSqPlt,
  // Decimator (Imperial)
  RAdmChiraneau, CaptOicunn, PatrolLdr,
  // EscapeCraft (Scum)
  ORPioneer, L337E, AutoPltDrone,
  // EWing (Rebel)
  CHorn, GDarklighter, RogueSqEsc, KnaveSqEsc,
  // FangFighter (Scum)
  FRau_FF, OTeroch, JRekkoff, KSolus, SkullSqPlt, ZealousRecruit,
  // Firespray (Scum)
  BFett, EAzzameen, KScarlet, KFrost, KTrelix, BountyHunter,
  // G1A (Scum)
  FourLOM, Zuckuss, GandFindsman,
  // GunBoat (Imperial)
  MajVynder, LtKarsabi, RhoSqPlt, NuSqPlt,
  // HWK290 (Rebel)
  JOrs, RGarnet, KKatarn, RebelScout,
  // HWK290 (Scum)
  DBonearm, PGodalhi, TMux, SpiceRunner,
  // JM5K (Scum)
  Dengar, TTrevura, Manaroo, ContractedSc,
  // Kihraxz (Scum)
  TCobra, Graz, VHel, BlackSunAce, CaptJostero, CartMarauder,
  // Kimogila (Scum)
  TKulda, DOberos_KG, CartExecution,
  // KWing (Rebel)
  MDoni, ETuketu, WardenSqPlt,
  // Lambda (Imperial)
  CaptKagi, ColJendon, LtSai, OmicronGrpPlt,
  // LancerClass (Scum)
  KOnyo, AVentress, SWren_LC, ShadowportHun,
  // M3A (Scum)
  Serissu, GRed, LAshera, QJast, TansariiPtVet, Inaldra, CartelSpacer, SBounder,
  // Quadjumper (Scum)
  ConstZuvio, SPlank, UPlutt, JGunrunner,
  // RZ1AWing (Rebel)
  JFarrell, ACrynyd, GreenSqPlt, PhoenixSqPlt,
  // Scurrg (Scum)
  CaptNym, SSixxa, LRevenant,
  // Sheathipede (Rebel)
  FRau_Sh, EBridger_Sh, ZOrrelios_Sh, AP5,
  // StarViper (Scum)
  Guri, DOberos_SV, PXizor, BSAssassin, BSEnforcer,
  // T65Xwing (Rebel)
  WAntilles,    LSkywalker, TKyrell,  GDreis_XW, JPorkins,  KSperado,
  BDarklighter, LTenza,     RedSqVet, BlueSqEsc, ETwoTubes, CAZealot,
  // TIEAdvV1 (Imperial)
  GrandInq, SeventhSister, BaronOfEmp, Inq,
  // TIEAdvX1 (Imperial)
  DVader, MStele, VFoslo, ZStrom, StormSqAce, TempestSqPlt,
  // TIEAggressor (Imperial)
  LtKestal, OnyxSqScout, DblEdge, SienSpecialist,
  // TIEBomber (Imperial)
  TBren, CaptJonus, MajRhymer, GammaSqAce, Deathfire, ScimitarSqPlt,
  // TIEDefender (Imperial)
  RBrath, ColVessery, CountessRyad, OnyxSqAce, DeltaSqPlt,
  // TIEFighter (Imperial)
  Howlrunner, MMithel, SSkutu,     DMeeko, GHask, IVersio,    SMarana,
  BlackSqAce, VRudor,  NightBeast, ObsidianSqPlt, AcademyPlt, Wampa,
  // TIEFighter (Rebel)
  EBridger_TF, SWren_TF, ZOrrelios_TF, CaptRex,
  // TIEInterceptor (Imperial)
  SFel, SaberSqAce, TPhennir, AlphaSqPlt,
  // TIEPhantom (Imperial)
  Whisper, Echo, SigmaSqAce, ImdaarTestPlt,
  // TIEPunisher (Imperial)
  Redline, Deathrain, CutlassSqPlt,
  // TIEReaper (Imperial)
  MajVermeil, CaptFeroph, Vizier, ScarifBsPlt,
  // TIEStriker (Imperial)
  Duchess, Countdown, PureSabacc, BlackSqScout, PlanetarySent,
  // UWing (Rebel)
  BRook, SGerrera, CAndor, MYarro, BTwoTubes, BlueSqScout, HTobber, PartRenegade,
  // VCX100 (Rebel)
  HSyndulla_VCX, KJarrus, Chopper, LothalRebel,
  // YT1300scum (Scum)
  HSoloScum, LCalrissianScum, L337C,
  // YT1300reb (Rebel)
  HSoloReb, LCalrissianReb, Chewbacca, OuterRimSmug,
  // YT2400 (Rebel)
  DRendar, Leebo, WildSpaceFrin,
  // YV666 (Scum)
  Bossk, MEval, LRazzi, TSlaver,
  // YWing (Rebel)
  NWexley_YW, DVander, HSalm, EVerlaine, GoldSqVet, GraySqBomber,
  // YWing (Scum)
  Kavil, DRenthal, HiredGun, CrymorahGoon,
  // Z95 (Rebel)
  ACracken, LtBlount, TalaSqPlt, BanditSqPlt,
  // Z95 (Scum)
  NSuhlak, BlackSunSoldier, KLeeachos, BinayrePirate, NashtahPup,
};

class PilotNotFound : public std::runtime_error {
 public:
  PilotNotFound(std::string pilot, std::string faction, std::string ship);
  PilotNotFound(Plt plt);
};

struct PilotSide {
  const std::string      pilotName;
  const std::string      pilotNameShort;
  const std::string      pilotSubtitle;
  const PriAttacks       attacks;
  const int8_t           agility;
  const int8_t           hull;
  const int8_t           shield;
  const Chargeable       charge;
  const Chargeable       force;
  const std::experimental::optional<ShipAbility> shipAbility;
  const ActionBar        actions;
  const UpgradeBar       upgrades;
  const bool             hasAbility;
  const std::string      text;
};



class Pilot {
 public:
  // factories
  static Pilot              GetPilot(Plt pilot);
  static std::vector<Pilot> FindPilot(std::string, Fac f=Fac::All, std::vector<Pilot> src=Pilot::pilots);
  static std::vector<Pilot> GetAllPilots(Fac faction=Fac::All);

  // maintenance
  static void SanityCheck();

  // info
  Plt         GetType()       const;
  std::string GetName()       const;
  std::string GetShortName()  const;
  std::string GetSubtitle()   const;
  Faction     GetFaction()    const;
  Ship        GetShip()       const;
  std::experimental::optional<ShipAbility> GetShipAbility() const;
  bool        HasAbility()    const;
  std::string GetText()       const;
  uint8_t     GetLimited()    const;
  bool        IsUnreleased()  const;

  // stats
  int8_t     GetNatInitiative() const;
  int8_t     GetModInitiative() const;
  PriAttacks GetNatAttacks()    const;
  PriAttacks GetModAttacks()    const;
  int8_t     GetNatAgility()    const;
  int8_t     GetModAgility()    const;
  int8_t     GetNatHull()       const;
  int8_t     GetModHull()       const;
  int8_t     GetNatShield()     const;
  int8_t     GetModShield()     const;
  Chargeable GetNatCharge()     const;
  Chargeable GetModCharge()     const;
  Chargeable GetNatForce()      const;
  Chargeable GetModForce()      const;
  int16_t    GetNatCost()       const;
  int16_t    GetModCost()       const;
  ActionBar  GetNatActions()    const;
  ActionBar  GetModActions()    const;
  Maneuvers  GetNatManeuvers()  const;
  Maneuvers  GetModManeuvers()  const;
  std::vector<Upg> GetNatPossibleUpgrades() const;
  std::vector<Upg> GetModPossibleUpgrades() const;

 private:
  // stats
  Plt     pilot;
  Fac     faction;
  Shp     ship;
  uint8_t limited;
  uint8_t initiative;

  // adjustable stats
  int16_t cost;
  std::vector<UpT> upgradeSlots;

  // state
  std::vector<Upgrade> appliedUpgrades;

  uint8_t curSide;
  std::vector<PilotSide> sides;

  // db
  static std::vector<Pilot> pilots;

  // ctor
  Pilot(Plt         t,
	Fac         f,
        Shp         s,
        uint8_t     l,
        int8_t      i,
	int16_t     c,
        PilotSide   p
      );
};

}
