#include "maneuver.h"
#include "shared.h"
#include <string>

namespace libxwing2 {

// Bearing
BearingNotFound::BearingNotFound(Brn b)         : runtime_error("Bearing not found (enum " + std::to_string((int)b) + ")") { }
BearingNotFound::BearingNotFound(std::string t) : runtime_error("Bearing not found (" + t + ")") { }

std::vector<Bearing> Bearing::bearings = {
  { Brn::LTurn,       "Left Turn",           "L Turn",   "↰", "{LTURN}"      },
  { Brn::LBank,       "Left Bank",           "L Bank",   "↖", "{LBANK}"      },
  { Brn::Straight,    "Straight",            "Straight", "↑", "{STRAIGHT}"   },
  { Brn::RBank,       "Right Bank",          "R Bank",   "↗", "{RBANK}"      },
  { Brn::RTurn,       "Right Turn",          "R Turn",   "↱", "{RTURN}"      },
  { Brn::KTurn,       "Koiogran Turn",       "K-Turn",   "K", "{KTURN}"      },
  { Brn::Stationary,  "Stationary",          "Stop",     "■", "{STATIONARY}" },
  { Brn::LSloop,      "Left Segnor’s Loop",  "L Sloop",  "↖", "{LSLOOP}"     },
  { Brn::RSloop,      "Right Segnor’s Loop", "R Sloop",  "↗", "{RSLOOP}"     },
  { Brn::LTroll,      "Left Tallon Roll",    "L TRoll",  "↰", "{LTROLL}"     },
  { Brn::RTroll,      "Right Tallon Roll",   "R TRoll",  "↱", "{RTROLL}"     },
  { Brn::RevLBank,    "Reverse Left Bank",   "Rev LB",   "↙", "{REVLBANK}"   },
  { Brn::RevStraight, "Reverse Straight",    "Rev Str",  "↓", "{REVSTR}"     },
  { Brn::RevRBank,    "Reverse Right Bank",  "Rev RB",   "↘", "{REVRBANK}"   },
};

Bearing Bearing::GetBearing(Brn typ) {
  for(Bearing b : Bearing::bearings) {
    if(b.GetType() == typ) {
      return b;
    }
  }
  throw BearingNotFound(typ);
}

Bearing Bearing::GetBearing(std::string text) {
  for(Bearing b : Bearing::bearings) {
    if(b.GetText() == text) {
      return b;
    }
  }
  throw BearingNotFound(text);
}

Brn         Bearing::GetType()      const { return this->type; }
std::string Bearing::GetName()      const { return this->name; }
std::string Bearing::GetShortName() const { return this->shortName; }
std::string Bearing::GetCharacter() const { return this->character; }
std::string Bearing::GetText()      const { return this->text; }

Bearing::Bearing(Brn         b,
                 std::string n,
                 std::string s,
                 std::string c,
                 std::string t)
  : type(b), name(n), shortName(s), character(c), text(t) { }



bool FindManeuver(Maneuvers maneuvers, uint8_t speed, Brn bearing, Maneuver &maneuver) {
  for(auto m : maneuvers) {
    if((m.speed == speed) && (m.bearing == bearing)) {
      maneuver = m;
      return true;
    }
  }
  return false;
}

}
