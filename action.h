#pragma once
#include "shared.h"
#include "difficulty.h"
#include <functional>
#include <list>
#include <stdexcept>
#include <vector>
#include <stdint.h>

namespace libxwing2 {


// Actions
enum class Act : uint32_t {
  None       = 0x0000,
  Focus      = 0x0001,
  Lock       = 0x0002,
  BarrelRoll = 0x0004,
  Reload     = 0x0008,
  Reinforce  = 0x0010,
  Boost      = 0x0020,
  Coordinate = 0x0040,
  Jam        = 0x0080,
  Evade      = 0x0100,
  Cloak      = 0x0200,
  RotateArc  = 0x0400,
  Calculate  = 0x0800,
  SLAM       = 0x1000,
  //Recover    = 0x0020,

};

Act operator|(Act a, Act b);
Act operator&(Act a, Act b);
Act operator+(Act a, Act b);
Act operator-(Act a, Act b);

class ActionNotFound : public std::runtime_error {
 public:
  ActionNotFound(Act a);
};

class Action {
 public:
  static Action GetAction(Act s);
  Act         GetType()       const;
  std::string GetName()       const;
  std::string GetShortName()  const;

 private:
  Act type;
  std::string name;
  std::string shortName;

  static std::vector<Action> actions;

  Action(Act         t,
         std::string n,
         std::string s);
};

struct SAct { // skilled action
  Dif difficulty;
  Act action;
};

bool operator ==(const SAct a, const SAct b);


typedef std::vector<std::list<SAct>> ActionBar;

}
